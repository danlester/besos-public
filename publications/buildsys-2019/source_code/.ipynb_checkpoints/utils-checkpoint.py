import os

def create_new_data_file(model_type, filename):
    filepath = os.path.join(NEW_DATA_DIR, model_type)
    if not os.path.exists(filepath):
        os.makedirs(filepath)
    return os.path.join(filepath, filename)
