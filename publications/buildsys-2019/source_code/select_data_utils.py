import random
import numpy as np
import pandas as pd


## ==========================
## GENERAL
## ===========================
def limit_time_ranges(months, hours, building_df):
    
    start_month, end_month = months
    start_hour, end_hour = hours
    
    # choose only preselected months and hours from the time-series
    limited_building_df = building_df[
        (building_df.index.month >= start_month) |
        (building_df.index.month <= end_month)]

    limited_building_df = limited_building_df[
        (limited_building_df.index.hour >= start_hour) |
        (limited_building_df.index.hour <= end_hour)]
    if len(limited_building_df) == 0:
        raise ValueError("No data")
        
    return limited_building_df


def split_continuous_intervals(index, minimum_intervals, timestep):
    interval = []
    intervals = [interval]

    for this_time, index in zip(index, range(len(index))):
        last_interval = intervals[-1]
        if index == 0:
            last_interval.append(this_time)
        else:
            last_time = last_interval[-1]
            if this_time == last_time + pd.Timedelta(timestep, 'm'):
                last_interval.append(this_time)
            else:
                new_interval = [this_time]
                intervals.append(new_interval)
    if len(intervals[0]) == 0:
        return []
    intervals_long_enough = []

    for interval in intervals:
        if len(interval) > minimum_intervals:
            intervals_long_enough.append(interval)

    return intervals_long_enough


## ==========================
## DECAY CURVES
## ===========================
def select_dc_intervals(months, hours, decay_curve_params, building_df, cols):
    indoor_temp_diff = 'T_in_dff'
    in_out_temp_diff = 'in_out_temp_diff'
    interesting = 'interesting'
    t_in, t_out, heat_energy = cols
    building_df = limit_time_ranges(months, hours, building_df)

    # make a copy
    building_df_copy = building_df.copy()
    building_df_copy['heating_runtime'] = np.where(building_df_copy[heat_energy] > 0, 1, 0)

    # select based on params
    (setpoint_derivative_threshold, T_out_stationarity, T_in_derivative_threshold, T_in_out_diff,
     proportion_heating, max_duration, min_duration, minimum_intervals, timestep) = decay_curve_params

    # find difference between time steps using earlier time step
    df_t_in = building_df_copy[t_in]
    building_df_copy[indoor_temp_diff] = df_t_in.diff()
    building_df_copy[in_out_temp_diff] = df_t_in - building_df_copy[t_out]

    # label whether it is a point that we're interested in
    interesting = (
            (building_df_copy[indoor_temp_diff] < T_in_derivative_threshold)
             & (building_df_copy["heating_runtime"] <= proportion_heating)
             & (building_df_copy[in_out_temp_diff] >= T_in_out_diff)
    )
    building_df_copy = building_df[interesting]

    # get the time stamps
    df_index = list(building_df_copy.index)
    interval_indices = split_continuous_intervals(df_index, minimum_intervals, timestep)
    return interval_indices


## ==========================
## ENERGY BALANCE
## ===========================
def select_eb_intervals(months, hours, building_df, energy_balance_params):
    """
    Select the time intervals that will be used to train the data.

    Each interval is subject to certain constraints, according to energy_balance_params.
    These constraints are discussed further in the paper.

    TODO - comment and clean up this function
    """
    (
        interval_amnt,
        duration,
        heating_runtime_upper_bound,
        heating_runtime_lower_bound,
        indoor_variance_lower_bound,
        _
     ) = energy_balance_params

    intervals = []
    start_hour, end_hour = hours
    limited_building_df = limit_time_ranges(months, hours, building_df)
    df_index = list(limited_building_df.index)
    intervals = split_continuous_intervals(df_index, 1, 5)
    np.random.shuffle(intervals)
    ranges = extract_eb_ranges(intervals, duration, interval_amnt, heating_runtime_upper_bound, 
                   heating_runtime_lower_bound, indoor_variance_lower_bound, building_df)
    return ranges


def extract_eb_ranges(
    intervals, duration, interval_amnt, heating_runtime_upper_bound, 
    heating_runtime_lower_bound, indoor_variance_lower_bound, df):
    selected_intervals = []
    # TODO - maybe add a few more loops to get more intervals, looping through this once may not get many
    for interval in intervals:
        start, end = interval[0], interval[-1]
        # limit the indexes to fit within the described duration
        allowable_offset = len(interval) - (duration * (60 // 5)) #TODO hardcoded time step
        if allowable_offset < 0:
            continue
       # select subset of intervals for this time period
        start_index = int(np.floor(np.random.random() * allowable_offset))
        selected_interval = interval[start_index:int(start_index + duration * 60 // 5)] #TODO remove hardcoded time step
        # check heating and indoor temperature variance
        check_df = df[selected_interval[0]:selected_interval[-1]]
        # check nans
        if (check_df['Thermostat_Temperature'].isna().all() | 
            check_df['T_out'].isna().all() | 
            check_df['auxHeat1'].isna().all()):
            # skip if values are null
            continue
        if ((check_df['auxHeat1'].mean() > heating_runtime_upper_bound) or
            (check_df['auxHeat1'].mean() < heating_runtime_lower_bound)):
            # skip if there is too much or too little heating
            continue
        if check_df['Thermostat_Temperature'].var() < indoor_variance_lower_bound:
            # skip if there is not enough indoor variance
            continue
        # save intervals
        selected_intervals.append(selected_interval)
        # stop looping once a sufficent amount were found
        if(len(selected_intervals) == interval_amnt):
         #       i = 50
            break
            
    return selected_intervals
